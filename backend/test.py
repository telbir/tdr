import unittest
import numpy as np
import cv2
from keras.models import load_model

model = load_model('../tomatodisease2.h5')


class_names = {
    0: 'bacterial_spot',
    1: 'early_blight',
    2: 'late_blight',
    3: 'leaf_mold',
    4: 'septorial_leaf_spot',
    5: 'spotted spider mite',
    6: 'target spot',
    7: 'yellow leaf curl',
    8: 'mosaic_virus',
    9: 'healthy'
}


class MyTestCase(unittest.TestCase):
    def test_prediction(self):
        image = cv2.imread("../images/mosaicvirus-9_1000.jpg", 1)

        if image is not None:
            print("image loaded")
            image = cv2.resize(image, (256, 256), interpolation=cv2.INTER_AREA).astype(np.float32)
            # img = image
            image = np.expand_dims(image, axis=0)
            out = model.predict(image)
            print(class_names[np.argmax(out)])
            prediction = class_names[np.argmax(out)].__str__()

            self.assertIsNotNone(prediction)

if __name__ == '__main__':
    unittest.main()
