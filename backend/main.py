from flask import Flask, request, jsonify
from flask_cors import CORS, cross_origin
import numpy as np
import cv2
from keras.models import load_model
from PIL import Image
import tensorflow as tf

config = tf.ConfigProto()
config.gpu_options.allow_growth = True
session = tf.Session(config=config)
app = Flask(__name__)
CORS(app)

# All class names which the neural netwerk is trained on
class_names = {
    0: 'bacterial_spot',
    1: 'early_blight',
    2: 'late_blight',
    3: 'leaf_mold',
    4: 'septorial_leaf_spot',
    5: 'spotted spider mite',
    6: 'target spot',
    7: 'yellow leaf curl',
    8: 'mosaic_virus',
    9: 'healthy'
}

# Load the model
model = load_model('../tomatodisease2.h5')
model._make_predict_function()


@app.route('/')
def hello_world():
    return 'Hello, World!'


# Predict the disease based on incoming request
@app.route('/predict', methods = ['GET', 'POST'])
@cross_origin()
def predict():
    # image = cv2.imread("../images/mosaicvirus-9_1000.jpg", 1)
    image = request.files['file']
    image = Image.open(image).convert('RGB')
    image = np.array(image)

    if image is not None:
        print("image loaded")
        image = cv2.resize(image, (256, 256), interpolation=cv2.INTER_AREA).astype(np.float32)
        # img = image
        image = np.expand_dims(image, axis=0)
        out = model.predict(image)
        print(class_names[np.argmax(out)])
        prediction = class_names[np.argmax(out)].__str__()

        return jsonify(
            prediction=prediction)
    else:
        print("img not found")


hello_world()
app.run(debug = True)