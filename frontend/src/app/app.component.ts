import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

    url = 'http://127.0.0.1:5000/';
    uploadedFiles: Array<File>;
    prediction: String;

    constructor(private http: HttpClient) {
    }

    ngOnInit() {
    }

    fileChange(element) {
      this.uploadedFiles = element.target.files;
    }

    // Method to predict image
    upload() {
        let formData = new FormData();
        for (let i = 0; i < this.uploadedFiles.length; i++) {
            formData.append("file", this.uploadedFiles[i], this.uploadedFiles[i].name);
        }
        this.http.post(this.url + 'predict', formData)
            .subscribe(response => {
                console.log('response received is ', response);
                let resToJson = JSON.stringify(response);
                this.prediction = resToJson.slice(15, resToJson.length - 2);
                return this.prediction;
            })
    }
}
